## Resubmission 
This is a resubmission addressing comments received by Kurt Hornik (Email from 19 August 2022).
The issue leading to Note "Found the following HTML validation problems:
    air.density.html:65:38: Warning: nested emphasis <code>,..."
has been corrected by rewriting and regenerating the affected Rd files using the newest
CRAN version of roxygen2.


## Test environments
- aarch64-apple-darwin20 (64-bit); R version 4.2.1 (package development)
- checks on other environments using the rhub package:
  - Windows Server 2022, R-devel, 64 bit
  - macOS 10.13.6 High Sierra, R-release, CRAN's setup
  - Debian Linux, R-devel, GCC


## R CMD check results
0 errors, 0 warnings, 0 notes on all systems 

except for Windows Server 2022, R-devel, 64 bit, which showed 2 notes:

* checking sizes of PDF files under 'inst/doc' ... NOTE
Unable to find GhostScript executable to run checks on size reduction

* checking for detritus in the temp directory ... NOTE
Found the following files/directories:
  'lastMiKTeXException'

I assumed that both Notes are save to ignore as these types of notes do not occur 
when building and checking on CRAN. 



## Downstream dependencies
the package has one reverse Imports: REddyProc
R CMD check was run on this dependency using revdepcheck::revdep_check().
No problems were encountered.


